FROM node:18

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY prisma ./prisma

RUN npx prisma generate

COPY ./src ./src

COPY tsconfig.json ./
COPY tsconfig.build.json ./
COPY .env ./

RUN npm run build

CMD [ "npm", "run", "prod" ]